---
title: "JR01-Correlation Test"
author: "Jake Reed"
date: '`r Sys.Date()`'
output: 
  html_document:
    theme: simplex
    highlight: tango
    toc: yes
  pdf_document:
    highlight: tango
    toc: yes
---

# Design
## Background
The data for this analysis are means around the longest duration cycle of the clinical CLL data (poof), 
the fitted data the significance stat (Kstat), and a correlation matrix (log10(chi-squared pval)) of all 30 features. 

## Question
Is the overlap of the sector mean data around the longest duration loop also a correlation analysis? 

## Hypothesis
Due to the nature of the dimensionality reduction combined with sector mean values, we believe that the overlap/nonoverlap is
equivalent to correlation/noncorrelation. 

## Pseudocode
- convert all circle means values to a scale 0-1
- setup categories of means, for example 1-5
- calculate overlap based on categories of all features vs all features
- compare output to pearson correlation heatmap

# Setup Options
```{r opts, echo=FALSE}
knitr::opts_chunk$set(echo = TRUE, fig.width = 12, fig.height = 10)
options(width=96)
.format <- knitr::opts_knit$get("rmarkdown.pandoc.to")
.tag <- function(N, cap ) ifelse(.format == "html",
                                 paste("Figure", N, ":",  cap),
                                 cap)
```

# Load Libraries
```{r libraries, echo=FALSE}
library(ggplot2)
library(cowplot)
library(RPointCloud)
library(pheatmap)
```

# Load Data
```{r load_data, echo=FALSE}
source("00-paths.R")
load(file.path(paths$scratch, "circMean.RData"))
```

# Initial view
```{r fig1, echo=FALSE, fig.cap=.tag(1, "Circos plot of the top 7 features via Kstat")}
library(RColorBrewer)
suppressPackageStartupMessages( library(ComplexHeatmap) )
sigs <- names(sort(circMean@Kstat, decreasing = FALSE)[1:7])
poof[is.na(poof)] <- 0
angle.df <- poof[, sigs]
pal <- brewer.pal(7, "Set1")
col.ls <- lapply(seq(1:dim(angle.df)[2]), function(gene) {
  col <- pal[gene][[1]]
  c(Lo = "white", Hi = col)
})
cs <- col.ls
names(cs) <- colnames(angle.df)
lap <- packLegend(list = lapply(names(cs), function(N) {
  X <- cs[[N]]
  Legend(labels = names(X), title = N, legend_gp = gpar(fill = X))
}), direction = "horizontal", max_height = unit(1.5, "in"))
annote <- LoopCircos(cyc1, angle.df, col.ls)

opar <- par(mar = c(1, 0 , 3, 0))
image(x = annote)
title(main = "Clinical Features")
draw(lap, x = unit(0.5, "npc"), y = unit(0, "npc"),
  just = c("center", "bottom"))
par(opar)
rm(opar)

## Somethings fucky
colnames(poof)
poof[, c("Serum.beta.2.microglobulin", "LogB2M", "CatB2M")]
sigs <- c("Serum.beta.2.microglobulin", "LogB2M", "CatB2M", "Rai.Stage")
poof[is.na(poof)] <- 0
angle.df <- poof[, sigs]
pal <- brewer.pal(7, "Set1")
col.ls <- lapply(seq(1:dim(angle.df)[2]), function(gene) {
  col <- pal[gene][[1]]
  c(Lo = "white", Hi = col)
})
cs <- col.ls
names(cs) <- colnames(angle.df)
lap <- packLegend(list = lapply(names(cs), function(N) {
  X <- cs[[N]]
  Legend(labels = names(X), title = N, legend_gp = gpar(fill = X))
}), direction = "horizontal", max_height = unit(1.5, "in"))
annote <- LoopCircos(cyc1, angle.df, col.ls)

opar <- par(mar = c(1, 0 , 3, 0))
image(x = annote)
title(main = "Clinical Features")
draw(lap, x = unit(0.5, "npc"), y = unit(0, "npc"),
  just = c("center", "bottom"))
par(opar)
rm(opar)
```

```{r rescale}
head(poof)
scale <- apply(ss_poof, 2, function(col){
    #Rescale from 0 to 1
    ret <- (col - min(col)) / (max(col) - min(col))
    return(ret)
})
```

```{r cats}
cats <- apply(scale, 2, function(col) {
    # Categorize into 20 bins
    ret <- cut(col, breaks = 20, labels = c(1:20))
    return(ret)
})
```

```{r overlap}
# Calculate overlap by column against every other column
overlap <- apply(cats, 2, function(col) {
    ret <- apply(cats, 2, function(col2) {
        # Calculate overlap
        ret <- sum(col == col2) / length(col)
        return(ret)
    })
    return(ret)
})

# Rescale overlap to 0 - 20
overlap <- overlap * 20
```

```{r fig2, echo=FALSE, fig.cap=.tag(2, "Overlap of clinical features"), fig.width = 20, fig.height = 20}
source("/mnt/c/coombes_lab/TDA_plot_fxns.R")
lp <- pheat(LP)
ovr <- pheat(overlap)
#pe <- pheat(pear)
#sp <- pheat(spear)
plot_grid(lp, ovr, nrow = 2)
```

```{r correlation}
cor <- cor.test(LP, overlap)
cor

# pear.cor <- cor.test(pear, overlap)
# pear.cor
# spear.cor <- cor.test(spear, overlap)
# spear.cor
```


